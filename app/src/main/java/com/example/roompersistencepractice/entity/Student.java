package com.example.roompersistencepractice.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "students")
public class Student {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "stuName")
    public String name;
    @ColumnInfo(name = "stuId")
    public String stuId;
    @ColumnInfo(name = "image")
    String image;
    public Student() {
    }

    public Student(String name, String stuId, String image) {
        this.name = name;
        this.stuId = stuId;
        this.image = image;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", stuId='" + stuId + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
