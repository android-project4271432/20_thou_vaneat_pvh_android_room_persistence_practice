package com.example.roompersistencepractice.view;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.room.Room;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.roompersistencepractice.R;
import com.example.roompersistencepractice.base.BaseActivity;
import com.example.roompersistencepractice.dao.StudentDao;
import com.example.roompersistencepractice.database.AppDatabase;
import com.example.roompersistencepractice.databinding.ActivityMainBinding;
import com.example.roompersistencepractice.databinding.CustomMainBinding;
import com.example.roompersistencepractice.entity.Student;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.List;

public class MainActivity extends BaseActivity<ActivityMainBinding> {

    private static final String TAG = "MainActivity";
    private static final int PERMISSION_REQ_CODE = 100;
    private static final String PERMISSION_READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    private Uri selectedImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    //=============================Button upload image=======================================
        binding.imgView.setOnClickListener(view -> {
            Log.d(TAG, "onCreate: ");
            resultLauncher.launch(PERMISSION_READ_EXTERNAL_STORAGE);
        });

    //===============================Add data object student=====================================
        AppDatabase appDatabase= AppDatabase.getInstance(this);
        binding.btnAdd.setOnClickListener(view -> {

            String names = binding.txtEditName.getText().toString();
            String Id = binding.textEditId.getText().toString();
            String image = selectedImageUri != null ? selectedImageUri.toString() : "";
            Student student = new Student(names,Id,image);
            binding.txtEditName.setText(" ");
            binding.textEditId.setText(" ");
            appDatabase.studentDao().insertStudent(student);

            Toast.makeText(this, "Student was insert", Toast.LENGTH_SHORT).show();
        });

    //============================Dialog and get data show on UI==================================
        binding.textShow.setOnClickListener( v -> {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
            CustomMainBinding binding1 = CustomMainBinding.inflate(LayoutInflater.from(this));
            List<Student> allStudent = appDatabase.studentDao().getAllStudents();
            StringBuilder studentText = new StringBuilder();

            for (Student stu : allStudent){
                studentText.append(" Name : ").append(stu.name)
                        .append("\n\n");
            }
            builder.setTitle("Show All Data")
                    .setView(binding1.getRoot()).setMessage(studentText);
            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }
    //===============================Upload Image Permission=====================================
    private void openGallery() {
        Log.d(TAG, "openGallery: ");
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.addCategory(Intent.CATEGORY_OPENABLE);
        galleryIntent.setType("image/*");
        ActivityCompat.startActivityForResult(MainActivity.this, galleryIntent, PERMISSION_REQ_CODE, new Bundle());
    }
    private void requestPermission() {
        boolean isReadPermissionGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED;
        if (isReadPermissionGranted) {
            Log.d(TAG, "requestPermission: 1 ");
            openGallery();
        }
    }
    private final ActivityResultLauncher<String> resultLauncher = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            result -> {
                if (result) {
                    requestPermission();
                    Log.d(TAG, "ActivityResultLauncher: 1" + result);
                }
            });
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PERMISSION_REQ_CODE && resultCode == RESULT_OK) {
            selectedImageUri = data.getData();
            binding.imgView.setImageURI(selectedImageUri);
        }
    }

    //===============================Data binding=====================================
    @Override
    public int layout() {
        return R.layout.activity_main;
    }
}