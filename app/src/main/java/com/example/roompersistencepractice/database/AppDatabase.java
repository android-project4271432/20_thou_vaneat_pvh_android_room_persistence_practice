package com.example.roompersistencepractice.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.roompersistencepractice.dao.StudentDao;
import com.example.roompersistencepractice.entity.Student;

@Database(entities = Student.class, exportSchema = false, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public  abstract StudentDao studentDao();
    private static final String DB_NAME = "db_student";
    private  static AppDatabase instance;
    public static synchronized AppDatabase getInstance(Context context){
        if (instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), 
                            AppDatabase.class,DB_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }
}